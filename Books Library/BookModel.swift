//
//  BookModel.swift
//  Books Library
//
//  Created by Arindam on 08/02/20.
//  Copyright © 2020 Arindam. All rights reserved.
//

import Foundation

typealias Books = [BookModel]
struct BookModel: Codable {
    let id: String
    let book_title: String
    let author_name: String
    let genre: String
    let publisher: String
    let author_country: String
    let sold_count: Int
    let image_url: String
}
