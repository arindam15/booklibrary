//
//  BookDetailsTableViewCell.swift
//  Books Library
//
//  Created by Arindam on 08/02/20.
//  Copyright © 2020 Arindam. All rights reserved.
//

import UIKit

class BookDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var imageBook: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var lableAuthorName: UILabel!
    @IBOutlet weak var labelGener: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewContainer.layer.borderWidth = 1
        viewContainer.layer.borderColor = UIColor.lightGray.cgColor
        viewContainer.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
