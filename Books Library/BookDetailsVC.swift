//
//  BookDetailsVC.swift
//  Books Library
//
//  Created by Arindam on 08/02/20.
//  Copyright © 2020 Arindam. All rights reserved.
//

import UIKit

class BookDetailsVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var bookList = [BookModel]()
    var searchController : UISearchController!

    var tempBookList = [BookModel]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tempBookList = bookList
        self.searchController = UISearchController(searchResultsController:  nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.delegate = self
        self.searchController.searchBar.delegate = self
        self.searchController.hidesNavigationBarDuringPresentation = false
        self.searchController.dimsBackgroundDuringPresentation = false
        self.navigationItem.titleView = searchController.searchBar
        self.definesPresentationContext = true
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "BookDetailsTableViewCell", bundle: nil), forCellReuseIdentifier: "BookDetailsTableViewCell")
    }
 


}


extension BookDetailsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tempBookList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let bookdetailsCell = tableView.dequeueReusableCell(withIdentifier: "BookDetailsTableViewCell") as! BookDetailsTableViewCell
            bookdetailsCell.labelTitle.text         = tempBookList[indexPath.row].book_title
            bookdetailsCell.lableAuthorName.text    = tempBookList[indexPath.row].author_name
            bookdetailsCell.labelGener.text         = tempBookList[indexPath.row].genre
            bookdetailsCell.separatorInset = UIEdgeInsets(top: 0, left: tableView.frame.width, bottom: 0, right: 0)
            bookdetailsCell.imageBook.setImage(with: tempBookList[indexPath.row].image_url, placeholder: "")
            bookdetailsCell.selectionStyle = .none
        
        return bookdetailsCell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
}




extension BookDetailsVC: UISearchControllerDelegate, UISearchResultsUpdating,UISearchBarDelegate {
    func updateSearchResults(for searchController: UISearchController) {
        if let text = searchController.searchBar.text, !text.isEmpty {
            let fiterServices = bookList.filter({$0.book_title.lowercased().range(of: text.lowercased()) != nil})
            self.tempBookList = fiterServices
            tableView.reloadData()
        }
    }
    
    
    
    private func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
    }
}
