//
//  Extension.swift
//  Books Library
//
//  Created by Arindam on 08/02/20.
//  Copyright © 2020 Arindam. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView{
    func assignImageUsingCache(from imageURL: String){
        if let cachedImage = imageCache.object(forKey: imageURL as AnyObject) as? UIImage {
            self.image = cachedImage
            return
        }
        
        if let formattedImageUrl = URL(string: imageURL) {
            URLSession.shared.dataTask(with: formattedImageUrl, completionHandler: { (data, response, error) in
                guard error == nil else {
                    print("error in downloading image, error = \n\(error!)")
                    return
                }
                if let downloadedImage = UIImage(data: data!) {
                    DispatchQueue.main.async {
                        imageCache.setObject(downloadedImage, forKey: imageURL as AnyObject)
                        DispatchQueue.main.async {
                            self.image = downloadedImage
                        }
                    
                    }
                }
            }).resume()
        }
    }
    
    
    
}


extension UIImageView {
    func setImage(with urlString: String, placeholder: String){
        
        let newString = urlString.replacingOccurrences(of: " ", with: "%20", options: .literal, range: nil)
        
        
        let url = URL(string: newString)
        let processor = DownsamplingImageProcessor(size: self.frame.size)
        self.kf.indicatorType = .activity
        self.kf.setImage(
            with: url,
            placeholder: UIImage(named: placeholder),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
        
    }
}
