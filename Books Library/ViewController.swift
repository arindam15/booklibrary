//
//  ViewController.swift
//  Books Library
//
//  Created by Arindam on 08/02/20.
//  Copyright © 2020 Arindam. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    var bookTypeList: [BookModel]?
    var types = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        fetchBookList()
        tableView.tableFooterView = UIView()
    }
    
    
    fileprivate func fetchBookList(){
        if let path = Bundle.main.path(forResource: "sample", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                let decodedValue = try! decoder.decode(Books.self, from: data)
                self.bookTypeList = decodedValue
                for each in decodedValue{
                    if !types.contains(each.genre){
                        types.append(each.genre)
                    }
                }
                tableView.reloadData()
            } catch {
                // handle error
                print(error)
            }
        }
    }
    
}


extension ViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return types.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let titleCell = tableView.dequeueReusableCell(withIdentifier: "BookTitleTableViewCell") as? BookTitleTableViewCell {
            titleCell.separatorInset = UIEdgeInsets.zero
            titleCell.labelTitle.text = types[indexPath.row]
            titleCell.selectionStyle = .none
            return titleCell
        }
        else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sortedBook = bookTypeList?.filter({$0.genre == types[indexPath.row]})
        let bookDetailsVC = storyboard?.instantiateViewController(withIdentifier: "BookDetailsVC") as! BookDetailsVC
        bookDetailsVC.bookList = sortedBook!
        navigationController?.pushViewController(bookDetailsVC, animated: true)
    }
    
    
}
